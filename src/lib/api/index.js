const axios = require('axios');
const dotenv = require('dotenv');

dotenv.config({ path: '../../../' });

const api = axios.default;
api.defaults.params = {
  api_key: process.env.API_TOKEN,
};
api.defaults.baseURL = 'https://api.themoviedb.org/3';

module.exports = api;
